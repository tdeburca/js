import React, { Component } from 'react';
//import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="Map">
          { /*TODO*/ }
        </div>
        <div className="Controls">
          <Controls />
        </div>
      </div>
    );
  }
}

class Controls extends Component {
  render() {
    const stationlist = Object.keys(amtrak_lines)
    const listItems = stationlist.map((stationlist) =>
      <li>{stationlist} 
      <input type="checkbox" defaultChecked={amtrak_lines[stationlist]}/>
      </li>
    );
    return(
      <div>
        <ul>{listItems}</ul>
      </div>
    );
  }
}


export default App;

const amtrak_lines = {
  'Acela':null,
  'Adirondack':null,
  'Auto Train':null,
  'Blue Water':null,
  'California Zephyr':null,
  'Capitol Limited':null,
  'Cardinal':null,
  'Carolinian / Piedmont':null,
  'Cascades':null,
  'Chicago - St.Louis':null,
  'Coast Starlight':null,
  'DC – Richmond':null,
  'DC - Lynchburg - Roanoke':null,
  'DC - Newport News':null,
  'Empire Builder':null,
  'Empire Service':null,
  'Ethan Allen Express':null,
  'Heartland Flyer':null,
  'Hiawathas':null,
  'Hoosier State':null,
  'Illini(Illinois_Service)':null,
  'Illinois Zephyr':null,
  'Keystone Service':null,
  'Lake Shore Limited':null,
  'Maple Leaf':null,
  'NEC':null,
  'New Haven - Springfield':null,
  'Pacific Surfliner':null,
  'Pennsylvanian':null,
  'Regional':null,
  'San Joaquins':null,
  'Silver_Service / Palmetto':null,
  'Southwest Chief':null,
  'Sunset Limited':null,
  'The Downeaster':null,
  'Brightline':null,
  'City of New Orleans':null,
  'Crescent':null,
  'DC - Norfolk':null,
  'Kansas City - St.Louis(Missouri River Runner)':null,
  'Texas Eagle':null,
  'Pere Marquette':null,
  'Wolverines(M - DOT)':null,
  'Wolverines(Michigan_Services)':null,
  'Capitol Corridor':null,
  'Vermonter':null
}