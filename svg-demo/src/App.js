import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { SvgLoader, SvgProxy } from 'react-svgmt';

const svgUrl = "http://kartograph.org/showcase/usa-projection/usa.svg"

const App = () => (
  <div>
    <SvgLoader path={logo}>
      <SvgProxy selector="#middle" fill="green" />
      <SvgProxy selector="
      [data-label='bob']" fill="red" />
    </SvgLoader>
  </div>
);

export default App;
/*
What I need:
Map with Elements (all with id slector.):
  * Map of US.
  * Amtrack stations
  * Rail routes between the two.
*/
