import React, { Component } from 'react';
import amtrak_map from './amtrak3.svg';
import './App.css';
import { SvgLoader, SvgProxy } from 'react-svgmt';


class App extends Component {
  render() {
    return (
      <div className="App">
        <Map />
        <StationList />
      </div>
    );
  }
}

class StationList extends Component{
  getStation(){
    return(
      <div>
        {amtrak_lines.map((station) =>{
          return (
            <label>
              {station}
            <input
            name = {station}
            type = "checkbox"
            />
            </label>
          )
        })}
      </div>
    )
  }
  render() {
    return (
      <div>
        <form>
        {this.getStation()}
        </form>
      </div>
    )
  }
}

class Map extends Component {
  constructor(props) {
    super(props);
    this.state = {
      key : Math.floor(Math.random() * amtrak_lines.length)
    };
  }
  render() {
    const line = amtrak_lines[this.state.key]
    const selector_string = "[data-label='" + line + "']";
    return(
      <div>
        <SvgLoader path={amtrak_map}>
          <SvgProxy selector={selector_string} stroke="green" stroke-width="5"/>
        </SvgLoader>
        <h1> {line} {this.state.key}</h1>
      </div>
    );
  }

  tick = () => this.setState({
    key: Math.floor(Math.random() * amtrak_lines.length) 
  });

  componentDidMount() {
    this.timerID = setInterval(
      this.tick,
      1000
    );
  }
}

export default App;
//"[data-label='Vermonter']"

const amtrak_lines = [
  'Acela',
  'Adirondack',
  'Auto Train',
  'Blue Water',
  'California Zephyr',
  'Capitol Limited',
  'Cardinal',
  'Carolinian / Piedmont',
  'Cascades',
  'Chicago - St.Louis',
  'Coast Starlight',
  'DC – Richmond',
  'DC - Lynchburg - Roanoke',
  'DC - Newport News',
  'Empire Builder',
  'Empire Service',
  'Ethan Allen Express',
  'Heartland Flyer',
  'Hiawathas',
  'Hoosier State',
  'Illini(Illinois_Service)',
  'Illinois Zephyr',
  'Keystone Service',
  'Lake Shore Limited',
  'Maple Leaf',
  'NEC',
  'New Haven - Springfield',
  'Pacific Surfliner',
  'Pennsylvanian',
  'Regional',
  'San Joaquins',
  'Silver_Service / Palmetto',
  'Southwest Chief',
  'Sunset Limited',
  'The Downeaster',
  'Brightline',
  'City of New Orleans',
  'Crescent',
  'DC - Norfolk',
  'Kansas City - St.Louis(Missouri River Runner)',
  'Texas Eagle',
  'Pere Marquette',
  'Wolverines(M - DOT)',
  'Wolverines(Michigan_Services)',
  'Capitol Corridor',
  'Vermonter'
]