import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class GenericSlider extends Component {
  constructor(props) {
    super(props);
    this.handleSliderChange = this.handleSliderChange.bind(this);
  }

  handleSliderChange(e) {
    this.props.onSliderChange(e.target.value);
  }

  render() {
    return (
      <div className="GenericSlider">
        <input
          type="range"
          style={this.props.style}
          min={this.props.min}
          max={this.props.max}
          value={this.props.value}
          onChange={this.handleSliderChange}
        />
      </div>
    );
  }
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state= {
      sizeValue: 75,
      timingFunction: 20
    };

    this.handleSliderChange = this.handleSliderChange.bind(this);
    this.handleSlider2Change= this.handleSlider2Change.bind(this);
  }

  handleSliderChange(sizeValue) {
    this.setState({
      sizeValue: sizeValue,
    });
  }

  handleSlider2Change(timingFunction) {
    this.setState({
      timingFunction: timingFunction
    });
  }

  render() {
    const logoStyle = {
      height: this.state.sizeValue + 'px',
      animation: 'App-logo-spin infinite ' + this.state.timingFunction + 's linear'
    }
    // allow styling of slider to reverse direction.
    const rtlslide = {
      direction: 'rtl'
    }
    return (
      <div className="App">
        <header className="App-header">
          <img style={logoStyle} src={logo} className="App-logo" alt="logo" />
        </header>
        Size
        <GenericSlider 
          value={this.state.sizeValue}
          min='0'
          max='250'
          onSliderChange={this.handleSliderChange}
        />
        <p></p>
        <p></p>
        <p></p>
        <p></p>
        Timing 
        <GenericSlider 
          value={this.state.timingFunction}
          style={rtlslide} // reverse slider, move to right increases movemment speed.
          min='.5'
          max='25'
          onSliderChange={this.handleSlider2Change}
        />
      </div>
    );
  }
}



export default App;
