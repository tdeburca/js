import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class Slider extends Component {
  constructor(props) {
    super(props);
    this.handleSliderChange = this.handleSliderChange.bind(this);
  }

  handleSliderChange(e) {
    this.props.onSliderChange(e.target.value);
  }

  render() {
    return(
      <div class="slidecontainer">
        <input 
          type="range" 
          min="25" 
          max="200" 
          value={this.props.sliderValue}
          onChange={this.handleSliderChange}
        />
      </div>
    );
  }
}

class App extends Component {
  constructor(props) {
    super(props);
    this.state= {
      sliderValue: 75
    };

    this.handleSliderChange = this.handleSliderChange.bind(this);
  }

  handleSliderChange(sliderValue) {
    this.setState({
      sliderValue: sliderValue
    });
  }

  render() {
    const logoStyle = {
      height: this.state.sliderValue + 'px',
      animation: 'App-logo-spin infinite 2s linear'
    }
    return (
      <div className="App">
        <header className="App-header">
          <img style={logoStyle} src={logo} className="App-logo" alt="logo" />
        </header>
        <Slider 
          sliderValue={this.state.sliderValue}
          onSliderChange={this.handleSliderChange}
        />
        <Slider
        />
      </div>
    );
  }
}



export default App;
